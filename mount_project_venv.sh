#!/usr/bin/env bash

git clone https://linamtoquica1@bitbucket.org/linamtoquica1/django_meiko.git
cd django_meiko
pip3 install virtualenv
virtualenv -p $(which python3) meiko_env
source meiko_env/bin/activate
pip install -r requirements.txt
python manage.py migrate
python manage.py runserver
