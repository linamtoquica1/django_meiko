#!/usr/bin/env bash

git clone https://linamtoquica1@bitbucket.org/linamtoquica1/django_meiko.git
cd django_meiko
pip install virtualenvwrapper
export WORKON_HOME=$HOME/.virtualenvs
export PROJECT_HOME=$HOME/
export VIRTUALENVWRAPPER_PYTHON=/usr/local/bin/python3
export VIRTUALENVWRAPPER_VIRTUALENV=/usr/local/bin/virtualenv
export VIRTUALENVWRAPPER_VIRTUALENV_ARGS='--no-site-packages'
source /usr/local/bin/virtualenvwrapper.sh
mkvirtualenv django_meiko -p $(which python3.6) -a .
pip install -r requirements.txt
python manage.py migrate
python manage.py runserver
