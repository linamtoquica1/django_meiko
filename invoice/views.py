from rest_framework import generics, filters, viewsets, status
from rest_framework.authentication import SessionAuthentication
from rest_framework.response import Response

from utils.rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated

from invoice.models import Order, Customer
from invoice.serializers import OrderSerializer, CustomerSerializer
from django_filters.rest_framework import DjangoFilterBackend


class OrderList(generics.ListCreateAPIView):
    """
    get:
    Return a list of all the existing orders.
    post:
    Create a new order instance.
    """
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    queryset = Order.objects.all()
    serializer_class = OrderSerializer
    filter_backends = (DjangoFilterBackend,)
    filter_fields = {'customer': ['exact'], 'date': ['range', 'exact']}


class CustomerViewSet(viewsets.ModelViewSet):
    """
    This viewset automatically provides `list`, `create`, `retrieve`,
    `update` and `destroy` actions.

    Additionally we also provide an extra `votes` action.
    """
    queryset = Customer.objects.all()
    serializer_class = CustomerSerializer
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        instance.delete(request.user)
        return Response(status=status.HTTP_204_NO_CONTENT)
