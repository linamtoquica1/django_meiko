"""
invoice URL Configuration
"""
from django.conf.urls import url
from django.urls import re_path, include
from rest_framework.routers import DefaultRouter

from invoice import views

app_name = 'invoice'

router = DefaultRouter()
router.register(r'customer', views.CustomerViewSet)

urlpatterns = [
    re_path(r'^order/$', views.OrderList.as_view()),
    re_path(r'^', include(router.urls)),
]
