from django.contrib.auth.models import User
from django.db.models import Sum, F
from rest_framework import serializers

from invoice.models import AvailableProduct, Customer, Order, OrderDetail, Product


class OrderSerializer(serializers.ModelSerializer):
    """
    order serializer.
    """
    total = serializers.SerializerMethodField(
        help_text='sum(quantity_i*product_price_i)')
    products = serializers.SerializerMethodField(
        help_text='comma separated list of products and quantities')
    order_details = serializers.JSONField(write_only=True,
                                          help_text='[{"product_id": 1,'
                                                    '"quantity": 2}]')

    def get_total(self, order):
        """
        Return total price of products in an order
        Args:
            order(object): Order from serializer

        Returns:
            price_total(int): Price total

        """
        order_details = OrderDetail.objects.filter(order=order).aggregate(
            total=Sum(F('product__price') * F('quantity'))
        )
        return order_details['total']

    def get_products(self, order):
        """
        Return products of an order
        Args:
            order(Order): Order from serializer

        Returns:
            products(str): Products separated with ,
        """

        product_names = OrderDetail.objects.filter(order=order).values_list("product__name", flat=True)
        if product_names:
            return ", ".join(product_names)

    def create(self, validated_data):
        """
        Create and return a new `Order` instance, given the validated data.

        Args:
            validated_data(dict): data to create an order and order detail

        Returns:
            order(Order): Instance of order

        """
        validated_data['created_by'] = self.context['request'].user
        order_details = validated_data.pop("order_details")
        pass_validation, response_validate = self._validate_order(
            customer=validated_data['customer'],
            order_details=order_details
        )

        if pass_validation:
            order = Order.objects.create(**validated_data)
            for order_detail in response_validate:
                order_detail['created_by'] = self.context['request'].user
                OrderDetail.objects.create(
                    order=order, **order_detail
                )

            return order
        else:
            raise serializers.ValidationError({"error_detail": response_validate}, code='400')

    def _validate_order(self, customer, order_details):
        validated_orders = []
        for order_detail in order_details:
            available_product = AvailableProduct.objects.filter(
                customer=customer,
                product_id=order_detail.get("product_id")
            ).count()
            if available_product > 0:
                validated_orders.append(order_detail)

        if len(validated_orders) > 5:
            return False, "Ha excedido el maximo de productos permitidos."
        if len(validated_orders) == 0:
            return False, "Debe seleccionar por lo menos un producto para realizar el pedido"

        return True, validated_orders

    def update(self, instance, validated_data):
        """
        Update and return an existing `Order` instance, given the validated data.
        Args:
            instance(Order): order
            validated_data(dict): dict with fields to update
        Returns:
            order(Order)
        """
        instance.delivery_address = validated_data.get('delivery_address', instance.delivery_address)
        instance.updated_by = self.context['request'].user
        instance.save()
        return instance

    class Meta:
        model = Order
        fields = ('customer', 'id', 'delivery_address',
                  'date', 'total', 'products', 'order_details')


class CustomerSerializer(serializers.ModelSerializer):
    """
  customer serializer.
  """
    text = serializers.SerializerMethodField(help_text='customer name')

    def get_text(self, customer):
        return customer.name

    def create(self, validated_data):
        """
        Create and return a new `Customer` instance, given the validated data.

        Args:
            validated_data(dict): data to create an order and customer detail

        Returns:
            customer(Customer): Instance of customer

        """
        validated_data['created_by'] = User.objects.get(email="test@grupomeiko.com")
        customer = Customer.objects.create(**validated_data)

        return customer

    def update(self, instance, validated_data):
        """
        Update and return an existing `Customer` instance, given the validated data.
        Args:
            instance(Customer): customer
            validated_data(dict): dict with fields to update
        Returns:
            customer(Customer)
        """
        instance.name = validated_data.get('name', instance.name)
        instance.email = validated_data.get('email', instance.email)
        instance.updated_by = User.objects.get(email="test@grupomeiko.com")
        instance.save()
        return instance

    class Meta:
        model = Customer
        fields = ('id', 'text', "name", "email")
