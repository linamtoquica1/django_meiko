from django.contrib import admin

from invoice.models import Product, Customer, AvailableProduct, Order, OrderDetail


class ProductAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'price')
    readonly_fields = (
        "deleted_by", "deleted_date", "updated_by", "updated_date", "created_by", "created_date", "state"
    )

    def save_model(self, request, obj, form, change):
        if not change:
            obj.created_by = request.user
        else:
            obj.updated_by = request.user
        obj.save()

    def delete_model(self, request, obj):
        """
        Given a model instance delete it from the database.
        """
        obj.delete(request.user)

    def delete_queryset(self, request, queryset):
        """Given a queryset, delete it from the database."""
        queryset.delete(request.user)


class CustomerAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'email')
    readonly_fields = (
        "deleted_by", "deleted_date", "updated_by", "updated_date", "created_by", "created_date", "state"
    )

    def save_model(self, request, obj, form, change):
        if not change:
            obj.created_by = request.user
        else:
            obj.updated_by = request.user
        obj.save()

    def delete_model(self, request, obj):
        """
        Given a model instance delete it from the database.
        """
        obj.delete(request.user)

    def delete_queryset(self, request, queryset):
        """Given a queryset, delete it from the database."""
        queryset.delete(request.user)


class AvailableProductAdmin(admin.ModelAdmin):
    list_display = ('customer', 'product')
    readonly_fields = (
        "deleted_by", "deleted_date", "updated_by", "updated_date", "created_by", "created_date", "state"
    )

    def save_model(self, request, obj, form, change):
        if not change:
            obj.created_by = request.user
        else:
            obj.updated_by = request.user
        obj.save()

    def delete_model(self, request, obj):
        """
        Given a model instance delete it from the database.
        """
        obj.delete(request.user)

    def delete_queryset(self, request, queryset):
        """Given a queryset, delete it from the database."""
        queryset.delete(request.user)


class OrderAdmin(admin.ModelAdmin):
    list_display = ('customer', 'delivery_address', 'date')
    readonly_fields = (
        "deleted_by", "deleted_date", "updated_by", "updated_date", "created_by", "created_date", "state"
    )

    def save_model(self, request, obj, form, change):
        if not change:
            obj.created_by = request.user
        else:
            obj.updated_by = request.user
        obj.save()

    def delete_model(self, request, obj):
        """
        Given a model instance delete it from the database.
        """
        obj.delete(request.user)

    def delete_queryset(self, request, queryset):
        """Given a queryset, delete it from the database."""
        queryset.delete(request.user)


class OrderDetailAdmin(admin.ModelAdmin):
    list_display = ('order', 'product', 'quantity')
    readonly_fields = (
        "deleted_by", "deleted_date", "updated_by", "updated_date", "created_by", "created_date", "state"
    )

    def save_model(self, request, obj, form, change):
        if not change:
            obj.created_by = request.user
        else:
            obj.updated_by = request.user
        obj.save()

    def delete_model(self, request, obj):
        """
        Given a model instance delete it from the database.
        """
        obj.delete(request.user)

    def delete_queryset(self, request, queryset):
        """Given a queryset, delete it from the database."""
        queryset.delete(request.user)


admin.site.register(Product, ProductAdmin)
admin.site.register(Customer, CustomerAdmin)
admin.site.register(AvailableProduct, AvailableProductAdmin)
admin.site.register(Order, OrderAdmin)
admin.site.register(OrderDetail, OrderDetailAdmin)
