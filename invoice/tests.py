import datetime
import json

from django.contrib.auth.models import User
from django.test import TestCase, RequestFactory
import pycodestyle
from rest_framework import status
from rest_framework.exceptions import ErrorDetail
from rest_framework.test import APIClient, APIRequestFactory, force_authenticate, APITestCase

from invoice.factories import ProductFactory, CustomerFactory, OrderFactory, OrderDetailFactory, AvailableProductFactory
from invoice.models import OrderDetail, Order, Customer
from invoice.serializers import OrderSerializer
from invoice.views import OrderList


class DatoTestCase(TestCase):
    factory = APIClient()

    def test_login_get(self):
        response = self.factory.get('/api-token-auth/')
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_login_post_bad_request(self):
        response = self.factory.post('/api-token-auth/')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_login_post_ok(self):
        data = {
            "correo": "test@grupomeiko.com",
            "contrasenia": "d2hZ$u%5xvUc#2DY6ekLYiD#naXcjg%z"
        }
        response = self.factory.post('/api-token-auth/', data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_login_post_unprocessable_entity(self):
        data = {
            "correo": "test@grupomeiko.com",
            "contrasenia": "1sdf32fa1465aa879rwe"
        }
        response = self.factory.post('/api-token-auth/', data)
        self.assertEqual(response.status_code,
                         status.HTTP_422_UNPROCESSABLE_ENTITY)

    def test_customer_get_unauthorized(self):
        response = self.factory.get('/invoice/customer/')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_orders_get_unauthorized(self):
        response = self.factory.get('/invoice/order/')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_conformance(self):
        """Test that we conform to PEP-8."""
        style = pycodestyle.StyleGuide(quiet=False, config_file='mypy.ini')
        result = style.check_files(['invoice'])
        self.assertEqual(result.total_errors, 0,
                         "Found code style errors (and warnings).")


class OrderTestCase(TestCase):

    def setUp(self):
        self.request_factory = RequestFactory()
        self.user = User.objects.create(username='user1')

        self.product_1 = ProductFactory(
            name="Arroz",
            price=5000,
            product_description="Este es el producto arroz",
            created_by_id=1
        )
        self.product_2 = ProductFactory(
            name="Maiz",
            price=2000,
            product_description="Este es el producto maiz",
            created_by_id=1
        )

        self.customer = CustomerFactory(
            name="Luke Skywalker",
            email="luke.skywalker@gmail.com",
            created_by_id=1
        )

        self.order = OrderFactory(
            customer=self.customer,
            delivery_address="Av Siempre viva 123",
            date=datetime.datetime.now(),
            created_by_id=1
        )

        request = self.request_factory.get("/order/")
        request.user = self.user
        self.order_serializer = OrderSerializer(context={"request": request})

        OrderDetailFactory(order=self.order, product=self.product_1, quantity=2, created_by_id=1)
        OrderDetailFactory(order=self.order, product=self.product_2, quantity=1, created_by_id=1)

    def test_get_total_success(self):
        """ Test get total sum of products from an order  """

        assert self.order_serializer.get_total(order=self.order) == 12000

    def test_get_total_failed(self):
        assert self.order_serializer.get_total(None) is None

    def test_get_products_success(self):
        assert self.order_serializer.get_products(order=self.order) == 'Arroz, Maiz'

    def test_get_products_failed(self):
        assert self.order_serializer.get_products(None) is None

    def test_create_order_success(self):
        AvailableProductFactory(customer=self.customer, product=self.product_1, created_by_id=1)
        AvailableProductFactory(customer=self.customer, product=self.product_2, created_by_id=1)

        validated_data = {
          "customer": self.customer,
          "delivery_address": "Diagonal 86a # 101 - 40",
          "date": "2019-05-06",
          "order_details": [
            {
              "product_id": self.product_1.id,
              "quantity": 2
            },
            {
              "product_id": self.product_2.id,
              "quantity": 1
            }
          ]
        }

        order = self.order_serializer.create(validated_data)

        order_detail = OrderDetail.objects.filter(order=order).last()
        assert order.customer.name == "Luke Skywalker"
        assert order.date == "2019-05-06"
        assert order_detail.product.name == "Maiz"

    def test_create_order_avaliable_products(self):
        AvailableProductFactory(customer=self.customer, product=self.product_1, created_by_id=1)

        validated_data = {
          "customer": self.customer,
          "delivery_address": "Diagonal 86a # 101 - 40",
          "date": "2019-05-06",
          "order_details": [
            {
              "product_id": self.product_1.id,
              "quantity": 2
            },
            {
              "product_id": self.product_2.id,
              "quantity": 1
            }
          ]
        }

        order = self.order_serializer.create(validated_data)

        order_details = OrderDetail.objects.filter(order=order)
        order_detail = order_details.last()
        assert len(order_details) == 1
        assert order.customer.name == "Luke Skywalker"
        assert order.date == "2019-05-06"
        assert order_detail.product.name == self.product_1.name

    def test_create_order_with_more_than_five_products(self):
        AvailableProductFactory(customer=self.customer, product=self.product_1, created_by_id=1)

        validated_data = {
          "customer": self.customer,
          "delivery_address": "Diagonal 86a # 101 - 40",
          "date": "2019-05-06",
          "order_details": [
              {
                  "product_id": self.product_1.id,
                  "quantity": 2
              },
              {
                  "product_id": self.product_1.id,
                  "quantity": 2
              },
              {
                  "product_id": self.product_1.id,
                  "quantity": 2
              },
              {
                  "product_id": self.product_1.id,
                  "quantity": 2
              },
              {
                  "product_id": self.product_1.id,
                  "quantity": 2
              },
              {
                  "product_id": self.product_1.id,
                  "quantity": 2
              },
          ]
        }
        with self.assertRaises(Exception) as context:
            self.order_serializer.create(validated_data)
        self.assertTrue(
            str({'error_detail': ErrorDetail(string='Ha excedido el maximo de productos permitidos.', code='400')})
            in str(context.exception)
        )

    def test_create_order_without_products(self):
        AvailableProductFactory(customer=self.customer, product=self.product_1, created_by_id=1)

        validated_data = {
          "customer": self.customer,
          "delivery_address": "Diagonal 86a # 101 - 40",
          "date": "2019-05-06",
          "order_details": [
          ]
        }
        with self.assertRaises(Exception) as context:
            self.order_serializer.create(validated_data)

        self.assertTrue(str({'error_detail': ErrorDetail(string='Debe seleccionar por lo menos un producto '
                                                                'para realizar el pedido', code='400')})
                        in str(context.exception))

    def test_update_success(self):
        validated_data = {
            "delivery_address": "Calle Falsa 123",
            "customer_id": 2
        }
        order = self.order_serializer.update(self.order, validated_data)
        assert order.delivery_address == "Calle Falsa 123"
        assert order.customer == self.order.customer
        assert order.updated_by == self.user


class OrderAPITestCase(TestCase):
    factory = APIClient()

    def setUp(self):
        self.factory = APIRequestFactory()
        self.user = User.objects.create(username='test@grupomeiko.com')

        self.customer = CustomerFactory(
            name="Luke Skywalker",
            email="luke.skywalker@gmail.com",
            created_by_id=1
        )

        self.order = OrderFactory(
            customer=self.customer,
            delivery_address="Av Siempre viva 123",
            date=datetime.datetime.now(),
            created_by_id=1
        )

        self.product_1 = ProductFactory(
            name="Arroz",
            price=5000,
            product_description="Este es el producto arroz",
            created_by_id=1
        )

        OrderDetailFactory(order=self.order, product=self.product_1, quantity=2, created_by_id=1)

    def test_orders_get_orders(self):
        view = OrderList.as_view()

        request = self.factory.get('/invoice/order/')
        force_authenticate(request, user=self.user)
        response = view(request)
        assert response.status_code == status.HTTP_200_OK
        assert response.data


class OrderListCreateAPIViewTestCase(APITestCase):
    url = "/invoice/order/"

    def setUp(self):
        self.username = "john"
        self.email = "john@snow.com"
        self.password = "you_know_nothing"
        self.user = User.objects.create_user(self.username, self.email, self.password)
        self.api_authentication()
        self.customer = CustomerFactory(
            name="Luke Skywalker",
            email="luke.skywalker@gmail.com",
            created_by_id=1
        )

        self.order = OrderFactory(
            customer=self.customer,
            delivery_address="Av Siempre viva 123",
            date='2019-05-11',
            created_by_id=1
        )

        self.order_2 = OrderFactory(
            customer=self.customer,
            delivery_address="calle falsa 123",
            date='2019-05-12',
            created_by_id=1
        )

        self.product_1 = ProductFactory(
            name="Arroz prueba",
            price=5000,
            product_description="Este es el producto arroz",
            created_by_id=1
        )

        OrderDetailFactory(order=self.order, product=self.product_1, quantity=2, created_by_id=1)
        OrderDetailFactory(order=self.order_2, product=self.product_1, quantity=1, created_by_id=1)

    def api_authentication(self):
        data = {
            "correo": self.email,
            "contrasenia": self.password
        }
        response = self.client.post('/api-token-auth/', data)
        self.client.credentials(HTTP_AUTHORIZATION='bearer ' + response.data["token"])

    def test_create_order(self):
        AvailableProductFactory(customer=self.customer, product=self.product_1, created_by_id=1)
        response = self.client.post(self.url, data=json.dumps({
          "customer": self.customer.id,
          "delivery_address": "calle 88 ",
          "date": "2019-04-18",
          "order_details": [
            {
              "product_id": self.product_1.id,
              "quantity": 2
            }]}), content_type="application/json")
        self.assertEqual(201, response.status_code)
        assert json.loads(response.content)["products"] == "Arroz prueba"

    def test_list_orders(self):
        """
        Test to verify order list
        """

        response = self.client.get(self.url)
        self.assertTrue(response.status_code, 200)
        self.assertTrue(json.loads(response.content)["count"] == Order.objects.count())

    def test_list_order_by_customer(self):
        response = self.client.get(f"{self.url}?format=datatables&customer={self.customer.id}")
        self.assertTrue(response.status_code, 200)
        assert response.content == b'{"data":[{"customer":11,"id":17,"delivery_address":' \
                                   b'"Av Siempre viva 123","date":"2019-05-11","total":10000,' \
                                   b'"products":"Arroz prueba"},{"customer":11,"id":18,"delivery_address"' \
                                   b':"calle falsa 123","date":"2019-05-12","total":5000,"products"' \
                                   b':"Arroz prueba"}],"recordsFiltered":2,"recordsTotal":2,"draw":1}'

    def test_list_order_by_customer_not_existent(self):
        response = self.client.get(f"{self.url}?format=datatables&customer={1010101}")
        self.assertTrue(response.status_code, 200)
        assert json.loads(response.content) == \
            {"data":
                {"customer":
                    ["Select a valid choice. That choice is not one of the available choices."]
                 },
             "recordsFiltered": 1,
             "recordsTotal": 1,
             "draw": 1}

    def test_list_order_by_date_customer(self):
        response = self.client.get(f"{self.url}?format=datatables&customer={self.customer.id}&date=2019-05-11")
        self.assertTrue(response.status_code, 200)
        assert json.loads(response.content) == {'data': [{
            'customer': self.customer.id,
            'id': self.order.id,
            'delivery_address': 'Av Siempre viva 123',
            'date': '2019-05-11',
            'total': 10000,
            'products': 'Arroz prueba'
        }],
            'recordsFiltered': 1,
            'recordsTotal': 1,
            'draw': 1
        }

    def test_list_order_by_date_not_existent(self):
        response = self.client.get(f"{self.url}?format=datatables&date=2018-04-11")
        self.assertTrue(response.status_code, 200)
        assert json.loads(response.content) == {"data": [], "recordsFiltered": 0, "recordsTotal": 0, "draw": 1}

    def test_list_order_by_date_range_customer(self):
        response = self.client.get(
            f"{self.url}?format=datatables&customer={self.customer.id}&date__range=2019-05-10,2019-05-13"
        )
        self.assertTrue(response.status_code, 200)
        assert json.loads(response.content) == {
            "data":
                [
                    {
                        "customer": self.customer.id,
                        "id": self.order.id,
                        "delivery_address": "Av Siempre viva 123",
                        "date": "2019-05-11",
                        "total": 10000,
                        "products": "Arroz prueba"
                    },
                    {
                        "customer": self.customer.id,
                        "id": self.order_2.id,
                        "delivery_address": "calle falsa 123",
                        "date": "2019-05-12",
                        "total": 5000,
                        "products": "Arroz prueba"
                    }
                ],
            "recordsFiltered": 2,
            "recordsTotal": 2,
            "draw": 1
        }

    def test_list_order_by_date_range_customer_1(self):
        response = self.client.get(
            f"{self.url}?format=datatables&customer={self.customer.id}&date__range=2019-05-12,2019-05-13"
        )
        self.assertTrue(response.status_code, 200)
        assert json.loads(response.content) == {
            "data":
                [
                    {
                        "customer": self.customer.id,
                        "id": self.order_2.id,
                        "delivery_address": "calle falsa 123",
                        "date": "2019-05-12",
                        "total": 5000,
                        "products": "Arroz prueba"
                    }
                ],
            "recordsFiltered": 1,
            "recordsTotal": 1,
            "draw": 1
        }


class ClientAPITestCase(APITestCase):
    url = "/invoice/customer/"

    def setUp(self):
        self.username = "john"
        self.email = "john@snow.com"
        self.password = "you_know_nothing"
        self.user = User.objects.create_user(self.username, self.email, self.password)
        self.api_authentication()
        self.customer = Customer.objects.create(name="Maria Ramirez", email="email@gmail.com", created_by_id=1)

    def api_authentication(self):
        data = {
            "correo": self.email,
            "contrasenia": self.password
        }
        response = self.client.post('/api-token-auth/', data)
        self.client.credentials(HTTP_AUTHORIZATION='bearer ' + response.data["token"])

    def test_list_customers(self):
        response = self.client.get(self.url)
        self.assertTrue(response.status_code, 200)
        self.assertTrue(json.loads(response.content)["count"] == Customer.objects.count())

    def test_list_customer(self):
        response = self.client.get(f"{self.url}{self.customer.id}/")
        self.assertTrue(response.status_code, 200)
        assert json.loads(response.content) == {
            "id": 11,
            "text": "Maria Ramirez",
            "name": "Maria Ramirez",
            "email": "email@gmail.com"
        }

    def test_create_customer(self):

        response = self.client.post(self.url, data=json.dumps(
            {
                "name": "Luz Maria Toquica",
                "email": "lina.toquica@gmail.com"
            }
        ), content_type="application/json")
        self.assertEqual(201, response.status_code)
        assert json.loads(response.content)["name"] == "Luz Maria Toquica"

    def test_update_customer(self):
        response = self.client.put(f"{self.url}{self.customer.id}/", data=json.dumps({
            "name": "Maria Gonzales",
            "email": "email@gmail.com"
        }), content_type="application/json")
        self.assertEqual(200, response.status_code)
        assert json.loads(response.content)["name"] == "Maria Gonzales"
        assert json.loads(response.content)["email"] == "email@gmail.com"

    def test_delete_customer(self):
        response = self.client.delete(f"{self.url}{self.customer.id}/")
        self.assertEqual(204, response.status_code)
