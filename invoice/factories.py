import factory

from invoice.models import Product, Customer, AvailableProduct, Order, OrderDetail


class ProductFactory(factory.DjangoModelFactory):
    """
        Define Product Factory
    """
    class Meta:
        model = Product


class CustomerFactory(factory.DjangoModelFactory):
    """
        Define Customer Factory
    """
    class Meta:
        model = Customer


class AvailableProductFactory(factory.DjangoModelFactory):
    """
        Define AvailableProduct Factory
    """
    class Meta:
        model = AvailableProduct

    customer = factory.SubFactory(CustomerFactory)
    product = factory.SubFactory(ProductFactory)


class OrderFactory(factory.DjangoModelFactory):
    """
        Define AvailableProduct Factory
    """

    class Meta:
        model = Order

    customer = factory.SubFactory(CustomerFactory)


class OrderDetailFactory(factory.DjangoModelFactory):
    """
        Define OrderDetail Factory
    """

    class Meta:
        model = OrderDetail

    order = factory.SubFactory(OrderFactory)
    product = factory.SubFactory(ProductFactory)
